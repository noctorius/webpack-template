import * as $ from 'jquery'
//Просто подключенная стороняя либа например гугл или яндекс аналитика,
// тут может быть что угодно. и оно должно работать независимо от моего кода
function createAnalytics() {
    let counter = 0
    let isDestroyed = false

    const listener = () => counter++

    $(document).on('click', listener)
    return {
        destroy() {
            $(document).off('click', listener)
            isDestroyed = true
        },
        getClicks() {
            if(isDestroyed) {
                return `Analytics is destroyed. Total click = ${counter}`
            }
            return counter
        }
    }
}

window.analytics = createAnalytics()